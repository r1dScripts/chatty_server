Chatty server is a XMPP bot programmed to run on a server. It can be used

*    as a dynamic toto list.
*    to provide the travel time by car, foot and public transport between two adresses.
*    to give useful information about the load of the server.
*    to give the status of the server. The status of the account change depending on the load and memory usage.

The following sections describes the usage of `chatty_server`.
# When it start

At start, the program set a status base on the load and the memory usage.
```
18:40:29 server: Agayon.be en ligne
18:40:30 server: How you doin' ? Load: 0.12 0.05 0.05 Memory (Go): 1.08GiB/1.96GiB (35.2%)
18:40:30 server is now Available (How you doin' ? Load: 0.12 0.05 0.05 Memory (Go): 1.08GiB/1.96GiB (35.2%))
```
# Get the list of available commands

The list of command is send whenever the user send a message to the bot.
```
18:40:58 jnanar: help
18:40:58 Available Commands : status, reminder, travel, meteo
```
# Calculate a travel time:

Calculate a travel time is easy and fast. You need to define a Google Maps API key to use this function. You can also, define

*   your address,
*   the summary key: it is the summary of this travel( a highway, a long road, etc),
*   the shortest distance between work and home,
*   as well as the duration of this particular travel.

When the distance differs or time differs too much from these data, the bot will remind you to take another road.
```
18:22:22 jnanar: travel
18:22:23 memo: Usage: travel car start % stop
conveyance = car, bus, metro, tramway etc.
Example: travel car rue du parc 4000 Liège % Place saint Lambert 15 4000 Liège
```
# Examples:
```
18:42:08 jnanar: travel car rue du parc 4000 Liège % Place saint Lambert 15 4000 Liège
18:42:09 server: travel car
travel rue du parc 4000 liège to place saint lambert 15 4000 liège
Summary: Quai Orban
Distance: 2.46 km
Duration in traffic: 9.22 min
Duration (normal): 8.05 min
```
In order to use the travel time function you need to define an API key, a work location and a home location in the file named "libs/credentials.py". Some shortcuts are available: h2w and w2h for home and work location.
```
18:49:10 jnanar: travel h2w
18:49:11 server: home address to work address
Summary: E42
Distance: 7.81 km
Duration in traffic: 11.03 min
Duration (normal): 11.13 min
```
# Obtain the status of the server
```
18:34:13 jnanar: status
18:34:13 server:
Load: 2.12 2.26 2.31 Memory (Go): 3.94GiB/11.23GiB (17.6%)
CPUS : ('14.5%', '11.9%', '19.0%', '10.9%', '35.7%', '7.6%')
Memory (Go): 3.94GiB/11.23GiB (17.6%)
```
# Weather

First, you need to obtain an apikey. See the [official documentation](https://openweathermap.org/).

in my case, the command and message are configured in french but you can change this behaviour.

```
[18:25:29] jnanar: metéo
[18:25:30] server: À Liege : Température : 6°C, humidité: 93%

[18:26:20] jnanar: météo prévisions
[18:26:20] server: -----------------------------
Jour vendredi
Prévisions: Ciel dégagé pendant la nuit. Minimales : 1 ºC.
Précipitations: 10%
-----------------------------
Jour vendredi soir
Prévisions: Ciel dégagé pendant la nuit. Minimales : 1 ºC. Vents ESE et variables.
Précipitations: 10%
-----------------------------
Jour samedi
Prévisions: Partiellement nuageux. Maximales : 7 ºC. Vents ESE soufflant de 10 à 15 km/h.
Précipitations: 10%
-----------------------------
Jour samedi soir
Prévisions: Ciel plutôt dégagé. Risque de gel isolé. Minimales : 0 ºC. Vents ESE soufflant de 10 à 15 km/h.
Précipitations: 10%
-----------------------------
Jour dimanche
Prévisions: Ciel plutôt dégagé. Maximales : 7 ºC. Vents SSE soufflant de 10 à 15 km/h.
Précipitations: 10%
```


# Use it as a reminder or a TODOlist.
  
```
18:42:45 jnanar: reminder list
18:42:45 server: Todo list
18:42:57 jnanar: reminder write a blog article 5min
18:42:57 server: Reminder: write a blog article at 2017-01-04 18:48:04.797349
18:43:23 jnanar: reminder 2h program my personal robot
18:43:23 server: Reminder: program my personal robot at 2017-01-04 20:43:30.599113
18:43:29 jnanar: reminder list
18:43:29 server: Todo list
todo: due write a blog article at 2017-01-04 18:48:04.797349
todo: due program my personal robot at 2017-01-04 20:43:30.599113
18:43:57 jnanar: reminder 1min do a test
18:43:57 server: Reminder: do a test at 2017-01-04 18:45:04.779812
18:44:05 jnanar: reminder list
18:44:05 server: Todo list
todo: due write a blog article at 2017-01-04 18:48:04.797349
todo: due program my personal robot at 2017-01-04 20:43:30.599113
todo: due do a test at 2017-01-04 18:45:04.779812
18:45:01 server:
---
Reminder: do a test
---
18:45:10 jnanar: reminder list
18:45:10 server: Todo list
todo: due write a blog article at 2017-01-04 18:48:04.797349
todo: due program my personal robot at 2017-01-04 20:43:30.599113
todo: done do a test at 2017-01-04 18:45:04.779812

```

`chatty_server` is written in python v3 and is licensed under the GPLv3 licence.

# Install
## Install the dependencies (see requirements.txt)
*    SleekXMPP
*    Google Maps python API
    

## Clone the repository

    git clone git@gitlab.com:r1dScripts/chatty_server.git
    Rename config.example.ini, modify it and launch chatty_server.

```
$ python3 setup.py install
$ chatty_server -f $(pwd)/config.ini
$ chatty_server -f $(pwd)/config.ini  -d h2w
$ chatty_server -f $(pwd)/config.ini  -d w2h
```
