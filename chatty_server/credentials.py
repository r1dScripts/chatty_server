#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of ChattyServer.
#
# ChattyServer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ChattyServer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ChattyServer.  If not, see <http://www.gnu.org/licenses/>.
#

maps_api_key = "AIzaSyA86Mt0SqaBtdaM83rlDDWbU-SabBESm8w"

work_location = "Rue du travail 5 4460 Grâce-Hollogne Belgique"
home_location = "Rue Julien d'Andrimont 15 4000 Liège Belgique"
# The summary string if the best roads are taken (no traffic)
summary_key = "Big Autoroute name for example"
# The shortest distance (km)
d = 42
# the time threshold above which you get a warning (minutes)
t = 42
wunderground_api_key = "fd0687caca14c292"
weather_url = "http://api.wunderground.com/api/{}/forecast/conditions/lang:FR/q/Belgique/Liege.json".format(
    wunderground_api_key
)
