#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of ChattyServer.
#
# ChattyServer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ChattyServer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ChattyServer.  If not, see <http://www.gnu.org/licenses/>.
#

import googlemaps
from datetime import datetime

# gmaps = googlemaps.Client(key='Add Your Key here')
# start_location = ""
# stop_location = ""

ERROR_ADDRESS = "Erreur API. problème adresse"


class Google:
    def __init__(self, config):
        self.start = None
        self.stop = None
        self.gmaps = googlemaps.Client(key=config["maps_api_key"])
        self.config = config

    def get_start_stop_work(self, direction):
        start = None
        stop = None
        if direction == "w2h":
            direction = "work to home"
            start = self.config["work_location"]
            stop = self.config["home_location"]
        if direction == "h2w":
            direction = "home to work"
            start = self.config["home_location"]
            stop = self.config["work_location"]
        self.start = start
        self.stop = stop
        return True

    def get_route(self, start, stop, when, travel_mode):
        route = self.gmaps.directions(
            start,
            stop,
            mode=travel_mode,
            traffic_model="best_guess",
            departure_time=when,
        )
        return route

    def get_traject_work_home(self, direction):
        summary_msg = ""
        self.get_start_stop_work(direction)
        # Request directions via public transit
        now = datetime.now()
        travel_mode = ""
        route = self.get_route(self.start, self.stop, now, travel_mode)

        route_res = route[0]
        summary = route_res["summary"]
        direction_time = route_res["legs"][0]["duration_in_traffic"]
        distance = route_res["legs"][0]["distance"]

        summary_msg += "\n" + direction
        summary_msg += (
            "\n"
            + str(summary)
            + " "
            + "Distance: {} km \n".format((round(distance["value"] / 1000, 2)))
        )
        if self.config["summary_key"] not in summary:
            summary_msg += "Eviter " + self.config["summary_key"] + "\n"

        if distance["value"] > self.config["min_distance"] * 1000:
            summary_msg += "Chemin non habituel" + "\n"
        else:
            summary_msg += "Chemin habituel" + "\n"

        if direction_time["value"] >= self.config["min_time"] * 60:
            summary_msg += "Attention bouchons" + "\n"
        else:
            summary_msg += "Pas de soucis sur la route" + "\n"
        traffic_time = route_res["legs"][0]["duration_in_traffic"]["value"]
        normal_time = route_res["legs"][0]["duration"]["value"]
        summary_msg += (
            "temps traffic: {} min".format(round(traffic_time / 60.0, 2)) + "\n"
        )
        summary_msg += (
            "temps normal: {} min".format(round(normal_time / 60.0, 2)) + "\n"
        )

        return summary_msg

    def get_general_route(self, start, stop, travel_mode):
        summary_msg = ""
        now = datetime.now()
        route = None
        try:
            route = self.get_route(start, stop, now, travel_mode)
        except googlemaps.exceptions.ApiError:
            summary_msg = "APIError : " + ERROR_ADDRESS

        try:
            route_res = route and route[0]
            if not route_res:
                return ERROR_ADDRESS
            summary = route_res["summary"]
            try:
                direction_time = route_res["legs"][0]["duration_in_traffic"]
            except KeyError:
                direction_time = route_res["legs"][0]["duration"]
            distance = route_res["legs"][0]["distance"]
            summary_msg += "{} to {} \n".format(start, stop)
            summary_msg += (
                "Summary: "
                + str(summary)
                + "\n"
                + "Distance: {} km \n".format((round(distance["value"] / 1000, 2)))
            )
            traffic_time = direction_time["value"]
            normal_time = route_res["legs"][0]["duration"]["value"]
            summary_msg += (
                "Duration in traffic: {} min".format(round(traffic_time / 60.0, 2))
                + "\n"
            )
            summary_msg += (
                "Duration (normal): {} min".format(round(normal_time / 60.0, 2)) + "\n"
            )
        except IndexError:
            summary_msg = "No road available"
            pass
        return summary_msg

    def process_message(self, response, road_cmd):
        msg_content = ""

        if "w2h" in response:
            msg_content += self.get_general_route(
                self.config["work_location"], self.config["home_location"], "driving"
            )
            return msg_content

        if "h2w" in response:
            msg_content += self.get_general_route(
                self.config["home_location"], self.config["work_location"], "driving"
            )
            return msg_content

        if not "%" in response or not any(
            x in response for x in ["car", "foot", "bus", "bike"]
        ):
            msg_content = (
                "Usage: {} [car|foot|bus|bike] start % stop".format(road_cmd) + "\n"
            )
            msg_content += "bus = bus, metro, tramway etc. \n"
            msg_content += "Example: {} car rue du parc 4000 Liège % Place saint Lambert 15 4000 Liège".format(
                road_cmd
            )
            return msg_content

        if "%" in response:
            response_clean = response.replace("trajet", "").replace("travel", "")
            match = next(
                (x for x in ["car", "foot", "bus", "bike"] if x in response), False
            )
            response_clean = response_clean.replace(match, "")
            msg_content += "{} {} \n".format(road_cmd, match)
            address_list = response_clean.split("%")
            travel_mode = None
            if match == "car":
                travel_mode = "driving"
            if match == "foot":
                travel_mode = "walking"
            if match == "bus":
                travel_mode = "transit"
            if match == "bike":
                travel_mode = "bicycling"
            msg_content += self.get_general_route(
                address_list[0], address_list[1], travel_mode
            )
            return msg_content
