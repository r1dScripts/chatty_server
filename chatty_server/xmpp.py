#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of ChattyServer.
#
# ChattyServer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ChattyServer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ChattyServer.  If not, see <http://www.gnu.org/licenses/>.
#
import re
import slixmpp
from . import google_maps
from . import timer
from . import weather
from . import health

import unidecode
import datetime
import logging
import asyncio

logger = logging.getLogger(__name__)

remind_cmd = 'reminder'
road_cmd = 'travel'
weather_cmd = "meteo"


class SendMsgBot(slixmpp.ClientXMPP):
    """
    A basic Sleekxmpp bot that will log in, send a message,
    and then log out.
    """

    def __init__(self, jid, password, admin, config):
        slixmpp.ClientXMPP.__init__(self, jid, password)

        # The message we wish to send, and the JID that
        # will receive it.
        self.admin = admin
        self.admin_jid = slixmpp.xmlstream.JID(admin)
        self.config = config
        self.high_load = 0.0
        self.previous_load_maximum = 0.0
        self.timers = []

        self.mem_string = "Not set yet"
        self.memory = "Not set yet"
        self.load_string = "Not set yet"
        self.load = "Not set yet"
        self.mem_string = "Not set yet"
        self.load_maximum = "Not set yet"
        self.cpu_status = "Not set yet"
        self.load_string_status = "Not set yet"

        self.google = google_maps.Google(self.config['googlemaps'])
        self.message_to_send = ""

        # The session_start event will be triggered when
        # the bot establishes its connection with the server
        # and the XML streams are ready for use. We want to
        # listen for this event so that we we can initialize
        # our roster.
        self.add_event_handler("session_start", self.start)
        self.add_event_handler("message", self.message)

    def set_load_parameters(self, high_load, previous_load_maximum):
        self.high_load = high_load
        self.previous_load_maximum = previous_load_maximum

    def health(self, mem_string, memory, load_string, load, load_maximum, cpu_status, load_string_status):
        self.mem_string = mem_string
        self.memory = memory
        self.load_string = load_string
        self.load = load
        self.mem_string = mem_string
        self.load_maximum = load_maximum
        self.cpu_status = cpu_status
        self.cpu_status = 'CPUS : ' + str(self.cpu_status)
        self.load_string_status = load_string_status
        # cpu_process = get_process()

    async def start(self, event):
        """
        Process the session_start event.

        Typical actions for the session_start event are
        requesting the roster and broadcasting an initial
        presence stanza.

        Arguments:
            event -- An empty dictionary. The session_start
                     event does not provide any additional
                     data.
                     :param event:
        """
        self.send_presence()
        self.get_roster()

        if self.message_to_send:
            self.send_msg(self.message_to_send)
            await asyncio.sleep(2)
            self.disconnect()
            return
        else:
            # chatty loop
            self.send_msg("Agayon.be en ligne")
            await asyncio.sleep(2)
            h = health.Health(self)
            load_string, load = h.get_load()
            mem_string, memory = h.get_memory()
            load_string += " " + mem_string
            load_string_status = "How you doin' ? " + load_string
            self.send_msg(load_string_status)
            w = weather.Weather(self.config['weather'])
            weather_str = w.weather_now()
            self.send_msg(weather_str)
            self.set_status('available', load_string_status)
            while True:
                now = datetime.datetime.now()
                load_string, mem_string, load_maximum = h.get_health_and_set_status()
                logger.debug("Chatty Server: Load : " + load_string)
                logger.debug("Chatty Server: Memory : " + mem_string)
                h.previous_load_maximum = load_maximum
                for t in self.timers:
                    if t['state'] == "due" and t['when'] < now:
                        # It is time to send the reminder
                        remind_str = f"\n---------------------------------------------------\
                        \nReminder: {t.get('subject')}\n---------------------------------------------------"
                        self.send_msg(remind_str, recipient=t.get('recipient'))
                        t['state'] = "done"
                await asyncio.sleep(10)

    def send_msg(self, msg, recipient=None):
        if not recipient:
            recipient = self.admin
        self.send_message(mto=recipient,
                          mbody=msg,
                          mtype='chat')  # normal or chat

    def set_status(self, status, status_msg):
        self.send_presence(pstatus=status_msg, pshow=status)

    def send_status(self, recipient=None):
        if not recipient:
            recipient = self.admin
        msg = 'TODO'
        self.send_message(mto=recipient,
                          mbody=msg,
                          mtype='chat')  # normal or chat

    def message(self, msg):
        """
        Process incoming message stanzas. Be aware that this also
        includes MUC messages and error messages. It is usually
        a good idea to check the messages's type before processing
        or sending replies.

        Arguments:
            msg -- The received message stanza. See the documentation
                   for stanza objects and the Message stanza to see
                   how it may be used.
                   :param msg: the message
        """
        if msg['type'] in ('chat', 'normal'):
            message = msg['body']
            message = message.lower()
            unaccented_message = unidecode.unidecode(message.lower())
            sender = msg['from']
            sender_bare = sender.bare
            if sender.domain != self.admin_jid.domain:
                logger.info(f"Receive message from external user {sender}")
                return

            if 'status' in message:
                msg_content = '\n' + self.load_string + '\n' + self.cpu_status + '\n' + self.mem_string
                self.send_message(mto=msg['from'], mbody=msg_content, mtype='chat')  # normal or chat
                if "How you doin' ?" in self.load_string_status:
                    s = 'available'
                else:
                    s = 'dnd'
                self.set_status(s, self.load_string_status)

            elif remind_cmd in message:
                msg_usage = f"\nUsage:{remind_cmd} objet time\n"
                msg_usage += f"Ex: {remind_cmd} carwhash 1h"
                msg_usage += f"\n{remind_cmd} list: get a list of all reminders (due and done)"
                ti = timer.FamilyTimers()
                message = re.sub("\s\s+", " ", message)
                if message == remind_cmd + " list":
                    msg_content = "Todo list"
                    my_todo = [t for t in self.timers if t['recipient'] == sender_bare]
                    for todo in my_todo:
                        msg_content += f"\ntodo: {todo.get('state')} {todo.get('subject')} at {todo.get('when')}"
                    self.send_msg(msg_content, recipient=sender)
                    return
                todo = ti.process_message(message, sender.bare, remind_cmd)
                subject = todo.get('subject')
                when = todo.get('when')
                if not subject or not when:
                    msg_content = msg_usage
                    self.send_msg(msg_content, recipient=sender)
                    return
                else:
                    self.timers.append(todo)
                msg_content = f"Reminder: {subject} at {when}"
                # remove multiple spaces in the string
                msg_content = re.sub("\s\s+", " ", msg_content)
                self.send_msg(msg_content, recipient=sender)

            elif road_cmd in message:
                msg_content = self.google.process_message(message, road_cmd)
                self.send_msg(msg_content, recipient=sender_bare)
            elif weather_cmd in unaccented_message:
                w = weather.Weather(self.config['weather'])
                if 'prevision' in unaccented_message:
                    msg_content = w.forecast()
                else:
                    msg_content = w.weather_now()
                self.send_msg(msg_content, recipient=sender)
            else:
                msg_content = "Available Commands : status, {}, {}, {}".format(remind_cmd, road_cmd, weather_cmd)
                self.send_msg(msg_content, recipient=sender)
