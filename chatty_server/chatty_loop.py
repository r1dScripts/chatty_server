#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of ChattyServer.
#
# ChattyServer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ChattyServer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ChattyServer.  If not, see <http://www.gnu.org/licenses/>.
#

import logging
import time

from . import xmpp
from . import weather
from . import google_maps

logger = logging.getLogger(__name__)


class Chatty:
    def __init__(self, config):
        self.xmpp = None
        self.config = config
        self.jid = config["xmpp"]["jid"]
        self.password = config["xmpp"]["password"]
        self.admin = config["xmpp"]["admin"]
        self.google = None

    def prepare(self):

        epoch = int(time.time())
        self.jid += self.config["xmpp"]["ressource"] + str(epoch)
        logging.info("Start")
        #    print(jid,password, to)
        if self.jid is None or self.password is None or self.admin is None:
            logging.error("Chatty server: Error in config file")
            return "Error config not found"
        self.xmpp = xmpp.SendMsgBot(self.jid, self.password, self.admin, self.config)
        self.xmpp.register_plugin("xep_0030")  # Service Discovery
        self.xmpp.register_plugin("xep_0199")  # XMPP Ping
        self.xmpp.register_plugin("xep_0198")  # XMPP Ping

    def dailynews(self, direction_news, weather_news):
        summary_msg = ""
        if direction_news:
            self.google = google_maps.Google(self.config["googlemaps"])
            summary_msg += "\n" + self.google.process_message(direction_news, "travel")
        if weather_news:
            w = weather.Weather(self.config["weather"])
            summary_msg += "\n" + w.weather_now()
        self.prepare()
        self.xmpp.message_to_send = summary_msg
        self.xmpp.connect()
        self.xmpp.process(forever=False)
        logger.info("Dailinews: OK")

    def rocket_start(self):
        self.prepare()
        self.xmpp.connect()
        self.xmpp.process(forever=True)
