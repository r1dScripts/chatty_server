#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of ChattyServer.
#
# ChattyServer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ChattyServer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ChattyServer.  If not, see <http://www.gnu.org/licenses/>.
#

import configparser
import logging

logger = logging.getLogger(__name__)


def read_config(filename="config.example.ini"):
    config = configparser.ConfigParser()
    config.read(filename)
    section = "XMPP"
    jid = config.get(section, "jid")
    password = config.get(section, "password")
    ressource = config.get(section, "ressource")
    admin = config.get(section, "admin")
    section = "GOOGLEMAPS"
    maps_api_key = config.get(section, "maps_api_key")
    work_location = config.get(section, "work_location")
    home_location = config.get(section, "home_location")
    summary_key = config.get(section, "summary_key")
    min_distance = config.get(section, "min_distance")
    min_time = config.get(section, "min_time")
    section = "WEATHER"
    api_key = config.get(section, "api_key")
    weather_url = config.get(section, "weather_url")
    forecast_url = config.get(section, "forecast_url")

    conf = {
        "xmpp": {
            "jid": jid,
            "password": password,
            "ressource": ressource,
            "admin": admin,
        },
        "googlemaps": {
            "maps_api_key": maps_api_key,
            "work_location": work_location,
            "home_location": home_location,
            "summary_key": summary_key,
            "min_distance": min_distance,
            "min_time": min_time,
        },
        "weather": {
            "api_key": api_key,
            "weather_url": weather_url,
            "forecast_url": forecast_url,
        },
    }
    return conf


if __name__ == "__main__":
    current_conf = read_config("../config.example.ini")
    logger.info(current_conf)
