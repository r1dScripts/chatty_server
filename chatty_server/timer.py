#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of ChattyServer.
#
# ChattyServer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ChattyServer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ChattyServer.  If not, see <http://www.gnu.org/licenses/>.
#

import logging
import re
import datetime

logging.getLogger('root')


class Timer:
    def __init__(self):
        self.timer = {}

    def add_timer(self, when, subject, recipient):
        # status: done or due, datetime and the subject
        self.timer = {'state': "due", 'when': when, 'subject': subject, 'recipient': recipient}
        return self.timer


class FamilyTimers:
    def __init__(self):
        self.timers = []

    def get_timers(self):
        return self.timers

    def process_message(self, message, sender, remind_cmd='rappel', ):
        """
        process the message, extract number of hours, min and the object of the message
        :param message: input message
        :param remind_cmd: the name of the command (depend on the language)
        :return: the time when the remind must be done and the subject of the reminder
        """
        subject = message
        msg_content = ""
        # extract all numbers in the string
        ret_hours, ret_min = self.min_hours_detection(message)
        nbr_of_hours, message = self.get_nbr_hours(ret_hours, message)
        nbr_of_min, message = self.get_nbr_min(ret_hours, ret_min, message)
        try:
            subject = subject.replace(ret_hours[0][0], "")
        except IndexError:
            pass
        try:
            subject = subject.replace(ret_min[0][0], "")
        except IndexError:
            pass
        # delete remind_cmd
        subject = subject.replace(remind_cmd, "")
        # remove multiple spaces in the string
        subject = re.sub("\s\s+", " ", subject)
        dbg_str = message + " --> " + str(nbr_of_hours) + " HOURS and " + str(
            nbr_of_min) + " MIN" + " SUBJECT = " + subject
        logging.info(dbg_str)
        if nbr_of_min == 0 and nbr_of_hours == 0:
            return {}
        now = datetime.datetime.now()
        delta = datetime.timedelta(hours=nbr_of_hours, minutes=nbr_of_min)
        remind_time = now + delta
        timer = Timer()
        todo = timer.add_timer(remind_time, subject, sender)
        return todo

    def get_nbr_hours(self, ret_hours, message):
        """

        :param ret_hours:
        :param message:
        :return:
        """
        nbr_of_hours = 0
        try:
            nbr_of_hours = ret_hours[0]
            nbr_of_hours = nbr_of_hours[0]
            nbr_of_hours = nbr_of_hours.split('h')
            nbr_of_hours = nbr_of_hours[0]
            # print(ret_hours, "--> ", nbr_of_hours)
        except IndexError:
            pass

        try:
            nbr_of_hours = int(nbr_of_hours)
        except ValueError:
            logging.error(str(nbr_of_hours))
            pass

        return nbr_of_hours, message

    def get_nbr_min(self, ret_hours, ret_min, message):
        """

        :param ret_hours:
        :param ret_min:
        :param message:
        :return:
        """

        #       print(ret_hours, ret_min)
        nbr_of_min_tot = 0
        nbr_of_min = 0
        try:
            nbr_of_min = ret_hours[0]
            nbr_of_min = nbr_of_min[2]

        except IndexError:
            pass
        try:
            nbr_of_min_tot = int(nbr_of_min)
        except ValueError:
            logging.debug(str(nbr_of_min))
            pass

        nbr_of_min = 0
        try:
            nbr_of_min = ret_min[0]
            nbr_of_min = nbr_of_min[0]
            nbr_of_min = nbr_of_min.split('min')
            nbr_of_min = nbr_of_min[0]
        except IndexError:
            pass

        try:
            nbr_of_min = int(nbr_of_min)
        except ValueError:
            logging.debug(str(nbr_of_min))
            pass

        nbr_of_min_tot += nbr_of_min
        return nbr_of_min_tot, message

    def min_hours_detection(self, input_string):
        """

        :param input_string:
        :return:
        """
        # The r doesn't change the type at all, it just changes how the string
        # literal is interpreted. Without the r, backslashes are treated as escape characters.
        # With the r, backslashes are treated as literal. Either way, the type is the same.

        # search for 42h and 42h42 as well as 42min but also '42 h' and '42 min'
        hours_regex = re.compile(r'(\d+(\s)?h(\d+)?)')
        min_regex = re.compile(r'(\d+(\s)?min)')
        ret_hours = hours_regex.findall(input_string)
        ret_min = min_regex.findall(input_string)
        return ret_hours, ret_min
