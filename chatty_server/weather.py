#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of ChattyServer.
#
# ChattyServer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ChattyServer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ChattyServer.  If not, see <http://www.gnu.org/licenses/>.
#

import requests
import json
import logging

logger = logging.getLogger(__name__)


class Weather:
    def __init__(self, config):
        self.config = config

    def get_meteo(self, mode=None):
        api_key = self.config['api_key']
        url = None
        if mode == 'get_meteo':
            url = self.config['weather_url'].replace('api_key', api_key)
        if mode == 'forecast':
            url = self.config['forecast_url'].replace('api_key', api_key)
        http = requests.get(url)
        content = http.content
        json_string = content.decode(encoding='UTF-8')
        parsed_json = json.loads(json_string)
        return parsed_json

    def weather_now(self, ):
        parsed_json = self.get_meteo(mode='get_meteo')
        location = parsed_json['name']
        today_temp_c = parsed_json['main']['temp']
        relative_humidity = parsed_json['main']['humidity']
        try:
            wind_direction = parsed_json['wind']['deg']
        except KeyError:
            wind_direction = 0
            logging.error("Wine direction {}".format(parsed_json))
        wind_speed = parsed_json['wind']['speed']
        try:
            precipitations_1h = f"Précipitations dernière heure: {parsed_json['rain']['1h']} mm"
        except KeyError:
            precipitations_1h = ""
        try:
            description = parsed_json.get('weather')[-1] and parsed_json.get('weather')[-1].get('description', '')
        except (KeyError, IndexError):
            description = ''

        msg = f"À {location} : {description} Température : {today_temp_c}°C, humidité: {relative_humidity}%, " \
              f"vent {wind_direction}° de {wind_speed}km/h {precipitations_1h}"
        return msg

    def forecast(self):
        parsed_json = self.get_meteo(mode='forecast')
        full_data = []
        for day_num in range(7):
            data = {}
            d = parsed_json['list'][day_num]
            forecast = d['weather'][0]['description']
            data['day'] = day_num
            try:
                data['rain'] = str(d['rain']['3h']) + 'mm'  # Probability of Precipitation
            except KeyError:
                data['rain'] = 'aucune'
            temp = d['main']['temp']
            temp_min = d['main']['temp_min']
            temp_max = d['main']['temp_max']
            humidity = d['main']['humidity']
            data['forecast'] = "{}. Température {}° ({}° à {}°), {}% humidité,".format(
                forecast, temp, temp_min, temp_max, humidity)
            full_data.append(data)
        msg = ''
        for d in full_data:
            msg += '-----------------------------' + '\n'
            msg += "Jour {}".format(d['day']) + '\n'
            msg += "Prévisions: {}".format(d['forecast']) + '\n'
            msg += "Précipitations: {}".format(d['rain']) + '\n'
        return msg


def bulletin(data):
    for d in data:
        print('-----------------------------')
        print("Jour {}".format(d['day']))
        print("Prévisions: {}".format(d['forecast']))
        print("Précipitations: {}%".format(d['pop']))


if __name__ == '__main__':
    # w = weather.Weather({})
    config = {'api_key': 'aaa',
              'forecast_url': 'http://api.openweathermap.org/data/2.5/forecast?q=Liege,be&units=metric&mode=json&APPID=api_key',
              'weather_url': 'http://api.openweathermap.org/data/2.5/weather?q=Liege,be&units=metric&APPID=api_key'
              }
    w = Weather(config)
    ret = w.forecast()
    print(ret)
