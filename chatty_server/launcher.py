#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# """
# @author: Arnaud Joset
#
# This file is part of ChattyServer.
#
# ChattyServer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ChattyServer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ChattyServer.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import sys
import logging
import argparse

import chatty_server.chatty_loop as chatty_loop
from chatty_server import config_parser

logging.basicConfig(level=logging.INFO)
# work DIR is the directory where the script is
os.chdir(sys.path[0])

logger = logging.getLogger(__name__)


def config_retriever():
    parser = argparse.ArgumentParser(description="Hey, it's me, your Chatty server !")
    parser.add_argument(
        "-d",
        "--direction",
        dest="direction",
        metavar="",
        nargs="?",
        action="store",
        default=None,
        help="Direction: work to home or home to work"
        + "\n"
        + "h2w : home to work "
        + "\n"
        + "w2h : work to home",
    )
    parser.add_argument(
        "-w",
        "--weather",
        dest="weather",
        default=False,
        action="store_true",
        help="Weather forecast of the day",
    )

    parser.add_argument(
        "-f", "--file", help="Config file", required=True, default="config.ini"
    )

    args = parser.parse_args()
    logger.info(args)
    conf = config_parser.read_config(args.file)
    direction = args.direction
    weather = args.weather
    conf.update({"get_direction": direction, "get_weather": weather})
    return conf


def launcher():
    conf = config_retriever()
    if conf.get("get_direction") or conf.get("get_weather"):
        chatty = chatty_loop.Chatty(conf)
        chatty.dailynews(conf.get("get_direction"), conf.get("get_weather"))
        sys.exit(0)
    else:
        chatty = chatty_loop.Chatty(conf)
        chatty.rocket_start()


if __name__ == "__main__":
    launcher()
