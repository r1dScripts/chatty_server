#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# @author: Arnaud Joset
#
# This file is part of ChattyServer.
#
# ChattyServer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ChattyServer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ChattyServer.  If not, see <http://www.gnu.org/licenses/>.
#

import psutil
import os
import time

CPU_THRESHOLD = 1
MEMORY_THRESHOLD = 10


class Health:
    def __init__(self, xmpp):
        self.high_load = 1.4
        self.previous_load_maximum = 0.0
        self.xmpp = xmpp
        self.previous_expression = ""
        self.expression = ""
        # Baseline
        psutil.cpu_percent()

    def to_gib(self, bytes, factor=2**30, suffix="GiB"):
        """Converti un nombre d'octets en gibioctets.

        Ex : 1073741824 octets = 1073741824/2**30 = 1GiO
        :param factor:
        :param bytes:
        :param suffix:
        :param bytes:
        """
        return "%0.2f%s" % (bytes / factor, suffix)

    def get_memory(self):
        results = {}
        memory = psutil.virtual_memory()
        results["memory"] = (
            f"{self.to_gib(memory.active)}/{self.to_gib(memory.total)} ({memory.percent}%)"
        )
        mem_usage = results["memory"]
        mem_string = "Memory (Go):  %s" % mem_usage
        return mem_string, memory

    def get_cpu(self):
        results = {"cpus": tuple("%s%%" % x for x in psutil.cpu_percent(percpu=True))}
        return results["cpus"]

    def get_load(self):
        load = os.getloadavg()
        load_string = "Load: %s %s %s" % load
        return load_string, load

    def read_filetext(self, file):
        with open(file, "r") as f:
            text = f.read()
        return text

    def _get_big_cpu_processes_string(self):
        arg_list = ["pid", "name", "cpu_percent", "status", "cmdline", "memory_percent"]
        psutil.cpu_percent()
        time.sleep(0.1)
        values = [
            p
            for p in psutil.process_iter(attrs=arg_list)
            if p.info["cpu_percent"] > CPU_THRESHOLD
        ]
        memory_values = [
            p
            for p in psutil.process_iter(attrs=arg_list)
            if p.info["memory_percent"] > MEMORY_THRESHOLD
        ]
        cpu_vals = None
        memory_vals = None
        if values:
            sorted_values = sorted(
                values, key=lambda e: e.info["cpu_percent"], reverse=True
            )
            cpu_vals = []
            for p in sorted_values:
                cmdline = p.info["cmdline"]
                cpu_vals.append(
                    {
                        "name": p.info["name"],
                        "cpu_percent": p.info["cpu_percent"],
                        "cmdline": cmdline,
                    }
                )
        if memory_values:
            sorted_memory_values = sorted(
                memory_values, key=lambda e: e.info["memory_percent"], reverse=True
            )
            memory_vals = []
            for p in sorted_memory_values:
                cmdline = p.info["cmdline"]
                memory_vals.append(
                    {
                        "name": p.info["name"],
                        "memory_percent": p.info["memory_percent"],
                        "cmdline": p.info["cmdline"],
                    }
                )
        processes_string = ""
        if cpu_vals:
            processes_string += "\nProcess (High CPU):\n"
            for v in cpu_vals:
                cmdline = (
                    v["cmdline"][:3] + ["..."] if len(v["cmdline"]) > 3 else v["cmdline"]
                )
                cpu_str = f"{v['name']}: {v['cpu_percent']}% ({' '.join(cmdline[:3])}) \n"
                processes_string += cpu_str
        if memory_vals:
            processes_string += "\nProcess (High Memory):\n"
            for v in memory_vals:
                cmdline = (
                    v["cmdline"][:3] + ["..."] if len(v["cmdline"]) > 3 else v["cmdline"]
                )
                memory_str = f"{v['name']}: {v['memory_percent']}% ({' '.join(cmdline[:3])}) \n"
                processes_string += memory_str
        return processes_string

    def get_health_and_set_status(self):
        # Determine the load averages and the maximum of the 1, 10 and 15 minute
        # averages.
        # In case the maximum is above the "high_load" value, the status will be
        # set to "do not disturb" with the load averages as status message.
        # The status will be updated continuously once we reached high load.
        # When the load decreases and the maximum is below the "high_load" value
        # again, the status is reset to "available".
        mem_string, memory = self.get_memory()
        load_string, load = self.get_load()
        load_string += " "
        update_status = False
        load_maximum = max(list(load))
        cpu_status = self.get_cpu()

        load_string_status = "How you doin' ? " + load_string
        self.expression = "doin"
        if load_maximum >= self.high_load:
            load_string_status = "It's hot in here! " + load_string
            self.expression = "hot"
        if load_maximum >= 1.25 * self.high_load:
            load_string_status = "I am smoking! " + load_string
            self.expression = "smoking"
        if load_maximum >= 2 * self.high_load:
            load_string_status = "I am melting! " + load_string
            self.expression = "melting"
        if load_maximum >= 2.5 * self.high_load:
            load_string_status = "Kill me please! " + load_string
            self.expression = "kill"
        if load_maximum >= self.high_load:
            self.xmpp.set_status("dnd", load_string_status)
            update_status = True
        elif load_maximum < self.high_load <= self.previous_load_maximum:
            load_string_status = "How you doin' ? " + load_string
            self.xmpp.set_status("available", load_string_status)
            update_status = True

        load_string_status += self._get_big_cpu_processes_string()

        threshold = 0.1 * 1024 * 1024  # 100MB
        if memory.available < threshold:
            load_string_status = "I am out of memory! " + load_string
            self.xmpp.set_status("dnd", load_string_status)
            update_status = True
        if update_status is True and self.expression != self.previous_expression:
            self.xmpp.send_msg(load_string_status)
        self.xmpp.health(
            mem_string,
            memory,
            load_string,
            load,
            load_maximum,
            cpu_status,
            load_string_status,
        )
        self.previous_expression = self.expression
        return load_string, mem_string, load_maximum
